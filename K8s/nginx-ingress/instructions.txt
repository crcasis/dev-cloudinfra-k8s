Source: https://cloud.google.com/community/tutorials/nginx-ingress-gke

0.- Spin up a K8s cluster with minimum 2 nodes
1.- Install Helm
1.1.- 
> curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get > get_helm.sh
1.2.- 
> chmod 700 get_helm.sh
1.3.- 
> ./get_helm.sh

1.4.- 
> helm init
Verify it is installed:
> kubectl get deployments -n kube-system
Note: tiller should be there

2.- Deploy a test app in K8s
> kubectl run hello-app --image=gcr.io/google-samples/hello-app:1.0 --port=8080
> kubectl expose deployment hello-app

3.- install ingress controller along with the dependencies
> helm install --name nginx-ingress stable/nginx-ingress --set rbac.create=true
Verify it is installed:
> kubectl get service nginx-ingress-controller
and returns
-> /healthz that returns 200
-> / that returns 404

3.- Deploy ingress resource from ingress.yaml
> kubectl create -f ingress.yaml
Wait until the external IP address is available
> kubectl get ingress ingress-resource

4.- Test
1.- http://external-ip-of-ingress-controller/hello
2.- curl -H 'Host: test.demo.com' http://external-ip-of-ingress-controller/hello